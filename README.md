# demo-trace-client

## Description

Demo trace-xyz client

## Installation

We use yarn here since it's a little bit fast and the CLI needs one less word to run a command. 

```bash
$ yarn
```

## Configuration

Go to `src/config` and rename `hlf.config.example.ts` to `hlf.config.local.ts` and open that file.  
Change the parameters in the file to match your local configuration. 

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

