import { Module } from '@nestjs/common';
import { AppController } from 'controllers/app.controller';
import { AppService } from 'services/app.service';
import { FabricService } from 'services/fabric.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [FabricService],
})
export class AppModule {}
