import { Injectable, Logger } from '@nestjs/common';
import * as FabricClient from 'fabric-client';

import config from 'config/hlf.config';

@Injectable()
export class FabricService {
  constructor() {
    this.setup();
  }

  async setup(): Promise<void> {
    this.fabricClient = FabricClient.loadFromConfig(
      config.CONNECTION_PROFILE_PATH,
    );
    this.channel = this.fabricClient.getChannel();
    this.log(this.fabricClient.getUserContext);

    const stateStore = await FabricClient.newDefaultKeyValueStore({
      path: config.KEY_STORE_PATH,
    });
    this.fabricClient.setStateStore(stateStore);

    const cryptoSuite = FabricClient.newCryptoSuite();
    const cryptoStore = FabricClient.newCryptoKeyStore({
      path: config.KEY_STORE_PATH,
    });
    cryptoSuite.setCryptoKeyStore(cryptoStore);
    this.fabricClient.setCryptoSuite(cryptoSuite);

    /*
     * Although we do not do anything with the user returned from getUserContext, it is needed
     * because it internally calls loadUserFromStateStore and setUserContext.
     * -_- I know, right? Probably, singleton pattern implemented incorrectly.
     */
    const user = await this.fabricClient.getUserContext(config.USER_NAME, true);
    if (user && user.isEnrolled()) {
      this.log('User loaded. ');
    } else {
      throw new Error('User not found');
    }
  }

  fabricClient: FabricClient;
  channel: FabricClient.Channel;

  log(message: any): void {
    Logger.log(message, FabricService.name, true);
  }

  warn(message: any): void {
    Logger.warn(message, FabricService.name, true);
  }

  error(message: any, trace?: string): void {
    Logger.error(message, trace, FabricService.name, true);
  }

  async invoke({
    chaincodeId,
    fcn,
    args = [],
  }: {
    chaincodeId: string;
    fcn: string;
    args?: Array<string>;
  }): Promise<string> {
    this.log(`Starting to invoke chaincode ${chaincodeId}::${fcn}`);

    const proposalRequest = {
      chaincodeId,
      fcn,
      args,
      txId: this.fabricClient.newTransactionID(),
    };

    this.log(`Sending invoke request`);
    const proposalResult = await this.channel.sendTransactionProposal(
      proposalRequest,
    );
    const proposalResponses = proposalResult[0];
    const proposal = proposalResult[1];
    let isProposalGood = false;

    if (
      proposalResponses &&
      proposalResponses[0].response &&
      proposalResponses[0].response.status === 200
    ) {
      isProposalGood = true;
    }

    if (isProposalGood) {
      const commitRequest = {
        proposalResponses,
        proposal,
      };

      const txIdString = proposal.txId.getTransactionId();
      const promises = [];
      const sendPromise = this.channel.sendTransaction(commitRequest);
      promises.push(sendPromise);

      /* TODO Add eventhub code here */

      const [commitResult] = await Promise.all(promises);
      if (commitResult.status === 'SUCCESS') {
      } else {
        return JSON.stringify({ error: commitResult.status });
      }

      return commitResult;
    }
  }

  async query({
    chaincodeId,
    fcn,
    args = [],
  }: {
    chaincodeId: string;
    fcn: string;
    args?: Array<string>;
  }): Promise<string> {
    this.log(`Starting to query chaincode ${chaincodeId}::${fcn}`);

    const queryRequest = {
      chaincodeId,
      fcn,
      args,
      txId: null,
    };

    this.log(`Sending query request`);
    try {
      const queryResponses = await this.channel.queryByChaincode(queryRequest);
      this.log(`Received query response`);

      if (queryResponses && queryResponses.length === 1) {
        if (queryResponses[0] instanceof Error) {
          this.error(`Query error QE1: ${queryResponses[0]}`);
        } else {
          this.log(`Query successfull: ${queryResponses[0]}`);
          return queryResponses[0].toString();
        }
      } else {
        this.error(`Query error QE2: ${queryResponses}`);
        return JSON.stringify({ error: queryResponses });
      }
    } catch (error) {
      this.error(`Query error QE0: ${error}`);
      return JSON.stringify({ error });
    }
  }
}
