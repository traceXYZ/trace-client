import { Injectable, Logger } from '@nestjs/common';
import * as FabricClient from 'fabric-client';

import config from 'config/hlf.config';

@Injectable()
export class AppService {
  constructor() {
    this.setup();
  }

  async setup() {
    this.fabricClient = new FabricClient();
    this.channel = this.fabricClient.newChannel(config.CHANNEL_NAME);

    const orderer = this.fabricClient.newOrderer(config.ORDERERS[0]);
    const peer = this.fabricClient.newPeer(config.PEERS[0]);
    this.channel.addOrderer(orderer);
    this.channel.addPeer(peer);

    const stateStore = await FabricClient.newDefaultKeyValueStore({
      path: config.KEY_STORE_PATH,
    });
    this.fabricClient.setStateStore(stateStore);

    const cryptoSuite = FabricClient.newCryptoSuite();
    const cryptoStore = FabricClient.newCryptoKeyStore({
      path: config.KEY_STORE_PATH,
    });
    cryptoSuite.setCryptoKeyStore(cryptoStore);
    this.fabricClient.setCryptoSuite(cryptoSuite);

    const user = await this.fabricClient.getUserContext(config.USER_NAME, true);
    if (user && user.isEnrolled()) {
      Logger.log('User loaded. ', AppService.name);
    } else {
      throw new Error('User not found');
    }
  }

  fabricClient: any;
  channel: any;

  root(): string {
    return `Server time: ${new Date().toString()}`;
  }

  async query(query: any): Promise<any> {
    try {
      const queryResponses = await this.channel.queryByChaincode(query);
      Logger.log('Query completed. ', AppService.name);
      if (queryResponses && queryResponses.length === 1) {
        if (queryResponses[0] instanceof Error) {
          Logger.error(`Error from query: ${queryResponses[0]}`);
        } else {
          Logger.log(`Response: ${queryResponses[0].toString()}`);
          return queryResponses[0].toString();
        }
      }
    } catch (error) {
      Logger.error(`Error: ${error}`);
    }
  }

  async invoke(args: any): Promise<any> {
    const proposal = {
      ...args,
      txId: this.fabricClient.newTransactionID(),
    };

    const results = await this.channel.sendTransactionProposal(proposal);
    const proposalResponses = results[0];
    const justProposal = results[1];
    let isProposalGood = false;

    if (
      proposalResponses &&
      proposalResponses[0].response &&
      proposalResponses[0].response.status === 200
    ) {
      isProposalGood = true;
    } else {
    }

    if (isProposalGood) {
      const request = {
        proposalResponses,
        proposal: justProposal,
      };

      const txIdString = proposal.txId.getTransactionID();
      const promises = [];
      const sendPromise = this.channel.sendTransaction(request);
      promises.push(sendPromise);

      const eventHub = this.fabricClient.newEventHub();
      eventHub.setPeerAddr(config.EVENT_PEER);

      const txPromise = new Promise((resolve, reject) => {
        const handle = setTimeout(() => {
          eventHub.disconnect();
          reject({ eventStatus: 'TIMEOUT' });
        }, 3000);
        eventHub.connect();
        eventHub.registerTxEvent(
          txIdString,
          (tx, code) => {
            Logger.log(tx);
            clearTimeout(handle);
            eventHub.unregisterTxEvent(txIdString);
            eventHub.disconnect();

            const returnStatus = { status: code, txId: txIdString };
            if (code !== 'VALID') {
              reject(returnStatus);
            } else {
              resolve(returnStatus);
            }
          },
          err => {
            reject(new Error('Problem with eventhub'));
          },
        );
      });
      promises.push(txPromise);

      const [sendResult, txResult] = await Promise.all(promises);
      if (sendResult.status !== 'SUCCESS') {
        return {
          error: sendResult.status.toLowerCase(),
        };
      }

      return txResult;
    }
  }
}
