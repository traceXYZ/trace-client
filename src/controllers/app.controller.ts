import { Get, Controller, Post, Body, Logger } from '@nestjs/common';
import { AppService } from 'services/app.service';
import { FabricService } from 'services/fabric.service';

@Controller()
export class AppController {
  constructor(
    // private readonly appService: AppService,
    private readonly fabricService: FabricService,
  ) {}

  @Post('echo')
  async echo(@Body() content): Promise<any> {
    Logger.log(`Echo`, AppController.name);
    return content;
  }

  @Post('query')
  async query(@Body() content): Promise<any> {
    Logger.log(`Query call ${JSON.stringify(content)}`, AppController.name);
    return await this.fabricService.query(content);
  }

  @Post('invoke')
  async invoke(@Body() content): Promise<any> {
    Logger.log(`Invoke call ${JSON.stringify(content)}`, AppController.name);
    return await this.fabricService.invoke(content);
  }

  @Post('createUser')
  async createUser(@Body() content): Promise<any> {
    Logger.log(
      `Create ${content.isAdmin ? 'admin' : ''} user ${content.username}`,
    );
  }
}
