export default {
  KEY_STORE_PATH: '/home/johndoe/trace-xyz-core/scripts/hfc-key-store/',
  CHANNEL_NAME: 'mychannel',
  ORDERERS: ['grpc://localhost:7050'],
  PEERS: ['grpc://localhost:7051'],
  USER_NAME: 'user1',
  EVENT_PEER: 'grpc://localhost:7053',
  CONNECTION_PROFILE_PATH:
    '/home/johndoe/trace-xyz-core/config/connection_profile_local.json',
};
